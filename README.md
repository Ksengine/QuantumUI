<br />
<p align="center">
  <a href="https://github.com/QuantumGUI/QuantumUI">
    <img src="https://github.com/QuantumGUI.png" alt="Logo" width="80" height="80">
  </a>

  <h1 align="center">QuantumUI</h1>

  <p align="center">
    Modern GUI Toolkit For Python
    <br />
    <br />
    <a href="https://github.com/Quantum-GUI/QuantumUI"><strong>Explore the docs »</strong></a>
    <br />
    <a href="https://github.com/Quantum-GUI/QuantumUI">View Demo</a>
    ·
    <a href="https://github.com/QuantumGUI/QuantumUI/issues/new?assignees=Ksengine&labels=bug&template=bug_report.md&title=">Report Bug</a>
    ·
    <a href="https://github.com/QuantumGUI/QuantumUI/issues/new?assignees=Ksengine&labels=enhancement&template=feature_request.md&title=">Request Feature</a>
  </p>
</p>

[![CodeFactor](https://www.codefactor.io/repository/github/quantumgui/quantumui/badge)](https://www.codefactor.io/repository/github/quantumgui/quantumui) · 

<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

## About The Project

There are many GUI libraries available for Python.
- Python has built-in one - **Tkinter**(TCL/Tk based)
- Some of them are Native like.
- Some of them are Cross Platform.
- Some of Them are Modern.

**QuantumUI** is

- **Native like -** 
Quantum GUI widgets are native like. They are familiar to end user. But QuantumUI has many modern styles. Some other GUI libraries uses operating system-inspired theme over the top of a generic widget set. With them you can see weird looking GUI.
Always QuantumUI apps has native parts of Operating System specific apps. We'll look at `Quit` option.
  - on Linux and MacOS, its under Application menu
  - on Windows, its under File menu
  - on Android, iOS, Web, no Quit option.

- **Cross Platform**
  - <img src="https://www.microsoft.com/favicon.ico" alt="Windows" width="16" height="16"> Windows
  - **macOS**
  - <img src="https://www.linux.org/favicon.ico" alt="Linux" width="20" height="20"> Linux
  - :earth_asia: Web
  - **iOS**
  - <img src="https://www.android.com/favicon.ico" alt="Android" width="20" height="20"> Android
- **Modern**
  - Layout - HTML(Hyper Text Markup Language) based
  - Styles - CSS(Cascading Style Sheets) based
## Getting Started
### Prerequisites
#### Linux requirements
- GTK

or
- Qt

## Community
TODO

## Contributing
If you'd like to contribute to QuantumUI development, our guide for first time contributors will help you get started.

If you experience problems with QuantumUI, log them on GitHub. If you want to contribute code, please fork the code and submit a pull request.
