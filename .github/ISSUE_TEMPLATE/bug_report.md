---
name: Bug report
about: Create a report to help us improve
title: ''
labels: bug
assignees: Ksengine

---

**Describe the bug**
A clear and concise description of what the bug is.

**To Reproduce**
Steps to reproduce the behavior:
1. Run
```python
Your code goes here...
```
2. Go to '...'
3. Click on '....'
4. Scroll down to '....'
5. See error

**Expected behavior**
A clear and concise description of what you expected to happen.

**Screenshots**
If applicable, add screenshots to help explain your problem.

 - Device: [e.g. Raspberry Pi]
 - OS: [e.g. Raspbian vX.X]
 - Backend: [e.g. Web]
   - Browser(if Web) [e.g. stock browser, safari]
   - Native version[e.g. `quantumui.__nversion__`->3.10.3]
 - Version [e.g. 0.1.0]

**Additional context**
Add any other context about the problem here.
